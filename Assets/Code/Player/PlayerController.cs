using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]private float movementSpeed = 1f;

    private Animator animator;
    private Rigidbody2D rbody;
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        rbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    // Update is called once per frame
    private void Update()
    {
        Vector2 currentPos = rbody.position;
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        Vector2 inputVector = new Vector2(horizontalInput, verticalInput);
        inputVector = Vector2.ClampMagnitude(inputVector, 1);
        Vector2 movement = inputVector * movementSpeed;
        Vector2 newPos = currentPos + movement * Time.fixedDeltaTime;
        rbody.MovePosition(newPos);

        if(horizontalInput < 0)
        {
            spriteRenderer.flipX = true;
            animator.SetBool("WalkHorizontal", true);
            animator.SetBool("WalkDown", false);
            animator.SetBool("WalkUp", false);
        }
        else if(horizontalInput > 0)
        {
            spriteRenderer.flipX = false;
            animator.SetBool("WalkHorizontal", true);
            animator.SetBool("WalkDown", false);
            animator.SetBool("WalkUp", false);
        }
        else if(verticalInput < 0)
        {
            animator.SetBool("WalkHorizontal", false);
            animator.SetBool("WalkDown", true);
            animator.SetBool("WalkUp", false);
        }
        else if(verticalInput > 0)
        {
            animator.SetBool("WalkHorizontal", false);
            animator.SetBool("WalkDown", false);
            animator.SetBool("WalkUp", true);
        }
        else
        {
            animator.SetBool("WalkDown", false);
            animator.SetBool("WalkUp", false);
            animator.SetBool("WalkHorizontal", false);
        }
    }
}
